#include <stddef.h>

typedef unsigned char uchar;

int memcmp_(const void *s1, const void *s2, size_t n)
{
  const uchar *c1 = s1, *c2 = s2, *end; int d;
  for (end = c1 + n; c1 < end;)
    if ((d = (int) *c1++ - (int) *c2++) != 0) return d;
  return 0;
}

int memcmp_backwards(const void *s1, const void *s2, size_t n)
{
  const uchar *c1 = s1, *c2 = s2, *end; c1 += n-1; c2 += n-1; int d;
  for (end = s1; c1 >= end;)
    if ((d = (int) *c1-- - (int) *c2--) != 0) return d;
  return 0;
}

int memcmp_fast(const void *s1, const void *s2, size_t n)
{
  const size_t s = sizeof(size_t), *c1 = s1, *c2 = s2, *end;
  for (end = c1 + n/s; c1 < end; ++c1, ++c2)
    if (*c1 != *c2) return memcmp_(c1, c2, s);
  return memcmp_(c1, c2, n%s);
}

int memcmp_consttime(const void *s1, const void *s2, size_t n)
{
  // any way to improve?
  const uchar *c1 = s1, *c2 = s2, *end; int d = 0, d2;
  for (end = c1 + n; c1 < end;) {
    d2 = (int) *c1++ - (int) *c2++;
    if ((d2 != 0) & (d == 0)) d = d2;
  }
  return d;
}
