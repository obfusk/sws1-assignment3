#include <stdio.h>

#include "addvector.h"

int main ()
{
  int a[] = { 1, 2, 3 };
  int b[] = { 4, 5, 6 };
  int c[3];

  addvector(c, a, b, 3);
  for (size_t i = 0; i < 3; ++i) printf("%d\n", c[i]);

  return 0;
}
