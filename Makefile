.PHONY: all clean

SHELL     = bash
PROGRAMS  = exercise1 exercise1_ addvector_test memcmp_test
LIBS      = magic_function addvector memcmp

CC      = gcc
CFLAGS  = -Wall -Wextra -Werror -std=c99 -fno-stack-protector -g

all: $(PROGRAMS) exercise2.c

exercise1:      magic_function.o
exercise1_:     magic_function.o
addvector_test: addvector.o
memcmp_test:    memcmp.o

exercise2.c: addvector.c memcmp.c
	for file in $^; do echo "// $$file"; cat $$file; echo; done \
	  | sed -r 's/\<memcmp_\>/memcmp/' >$@

clean:
	for p in $(PROGRAMS); do rm -f "$$p"{,.o}; done
	for p in $(LIBS)    ; do rm -f "$$p".o   ; done
