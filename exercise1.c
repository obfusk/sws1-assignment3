#include <stdio.h>

#include "magic_function.h"

#define MAX     (1 + 4096 * 1024 / sizeof(size_t))
#define CANARY1 0x4242424242424242
#define CANARY2 0x3737373737373737
#define CANARY3 0x3742374237423742

int put_canary2 ()
{
  size_t a = CANARY2; return a;
}

/*
  determine stack space used by magic_function; brittle!

  assumptions:
    * stack grows down
    * last local allocated at bottom
    * magic_function does not use CANARY values
    * magic_function's stack frame does not end with memory that it
      allocates but does not write to (b/c this cannot be detected)
*/
int main ()
{
  size_t n, *p, *q, on_the_stack = CANARY3;
  //  printf("on_the_stack,q,p,n @ %p,%p,%p,%p\n",
  //    &on_the_stack, &q, &p, &n); // DEBUG
  for (p = &on_the_stack - 1; p >= &on_the_stack - MAX; --p)
    *p = CANARY1;
  put_canary2();
  for (p = &on_the_stack - 1; p >= &on_the_stack - MAX; --p)
    if (*p != CANARY2) { q = p; break; }
  magic_function();
  for (p = &on_the_stack - MAX; p <= q; ++p)
    if (*p != CANARY1) {
      n = sizeof(size_t) * (q - p + 1); break;
    }
  printf("magic_function's stack seems to be %zu bytes\n", n);
  //  for (p = &on_the_stack; p > &on_the_stack - 128; --p)
  //    printf("%016zx @ %p\n", *p, p); // DEBUG
  return 0;
}
