#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "magic_function.h"

#define MAX     (1 + 4096 * 1024 / sizeof(size_t))
#define CHECKS  100

size_t *p, *q, i, n = 0, c1, c2;

size_t max(size_t a, size_t b)
{
  return a > b ? a : b;
}

void fill_stack()
{
  size_t stack[MAX];
  for (p = stack; p < stack + MAX; ++p) *p = c1;
}

void check_stack()
{
  size_t stack[MAX];
  for (p = stack + MAX; *p != c2; ++p); q = p-1;
  for (p = stack; p < stack + MAX; ++p) if (*p != c1) break;
}

/*
  determine stack space used by magic_function; brittle!

  assumptions:
    * stack grows down
    * magic_function does not happen to use the random canary values
    * magic_function's stack frame does not end with memory that it
      allocates but does not write to (b/c this cannot be detected)
*/
int main ()
{
  size_t c3;
  srand(time(NULL));
  for (i = 0; i < CHECKS; ++i) {
    c3 = c2 = rand(); c1 = rand();
    fill_stack(); magic_function(); check_stack();
    n = max(n, (q - p) * sizeof(size_t));
  }
  printf("magic_function's stack seems to be %zu bytes\n", n);
  return 0;
  n = c3; /* MUST. USE. c3 */
}
