#ifndef ADDVECTOR_H
#define ADDVECTOR_H

void addvector(int *r, const int *a, const int *b, unsigned int len);

#endif
