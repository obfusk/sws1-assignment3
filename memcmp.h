#ifndef MEMCMP__H
#define MEMCMP__H

int memcmp_         (const void *s1, const void *s2, size_t n);
int memcmp_backwards(const void *s1, const void *s2, size_t n);
int memcmp_fast     (const void *s1, const void *s2, size_t n);
int memcmp_consttime(const void *s1, const void *s2, size_t n);

#endif
