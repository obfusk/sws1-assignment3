#include <stdlib.h>

/*
  magic function
*/
int magic_function ()
{
  //  size_t foo[100]; foo[50] = 0xdeadbeef;
  //  return foo[50];
  size_t a = 0xdeadbeef, b = 0xcafebabe, c[] = { 1, 2 };
  return (a & b) ^ (c[0] | c[1]);
}
