void addvector(int *r, const int *a, const int *b, unsigned int len)
{
  for (const int *end = a + len; a < end;) *r++ = *a++ + *b++;
}
