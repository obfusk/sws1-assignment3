#include <stdio.h>

#include "memcmp.h"

int main ()
{
  int a[] = { 1, 2, 3, 4 , 5, 9 };
  int b[] = { 1, 2, 3, 42, 5, 6 };
  int c[] = { 1, 2, 3, 4 , 5, 7 };
  size_t n = sizeof(a);

  printf("memcmp_ says %d\n", memcmp_(a, a, n));
  printf("memcmp_ says %d\n", memcmp_(a, b, n));
  printf("memcmp_ says %d\n", memcmp_(b, a, n));
  printf("memcmp_ says %d\n", memcmp_(a, c, n));

  printf("memcmp_backwards says %d\n", memcmp_backwards(a, a, n));
  printf("memcmp_backwards says %d\n", memcmp_backwards(a, b, n));
  printf("memcmp_backwards says %d\n", memcmp_backwards(b, a, n));
  printf("memcmp_backwards says %d\n", memcmp_backwards(a, c, n));

  printf("memcmp_fast says %d\n", memcmp_fast(a, a, n));
  printf("memcmp_fast says %d\n", memcmp_fast(a, b, n));
  printf("memcmp_fast says %d\n", memcmp_fast(b, a, n));
  printf("memcmp_fast says %d\n", memcmp_fast(a, c, n));

  printf("memcmp_consttime says %d\n", memcmp_consttime(a, a, n));
  printf("memcmp_consttime says %d\n", memcmp_consttime(a, b, n));
  printf("memcmp_consttime says %d\n", memcmp_consttime(b, a, n));
  printf("memcmp_consttime says %d\n", memcmp_consttime(a, c, n));

  return 0;
}
